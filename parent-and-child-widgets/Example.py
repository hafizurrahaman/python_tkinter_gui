import tkinter as tk

# Create the main window (root widget)
root = tk.Tk()
root.title("Parent and Child Widgets Example")

# Create a parent frame widget
parent_frame = tk.Frame(root, bg="lightblue", width=300, height=200)
parent_frame.pack(padx=10, pady=10)

# Create a child label widget inside the parent frame
child_label = tk.Label(parent_frame, text="I am a child widget", bg="yellow")
child_label.pack(padx=20, pady=20)

# Create another child button widget inside the parent frame
child_button = tk.Button(parent_frame, text="Click Me")
child_button.pack(padx=20, pady=20)

# Start the Tkinter event loop
root.mainloop()
