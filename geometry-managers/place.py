import tkinter as tk

root = tk.Tk()
root.title("Place Example")

# Create widgets
label1 = tk.Label(root, text="Label 1", bg="red")
label2 = tk.Label(root, text="Label 2", bg="green")
label3 = tk.Label(root, text="Label 3", bg="blue")

# Place widgets
label1.place(x=50, y=50, width=100, height=30)
label2.place(x=200, y=50, width=100, height=30)
label3.place(x=125, y=100, width=100, height=30)

root.mainloop()
