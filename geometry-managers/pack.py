import tkinter as tk

root = tk.Tk()
root.title("Pack Example")

# Create widgets
label1 = tk.Label(root, text="Label 1", bg="red")
label2 = tk.Label(root, text="Label 2", bg="green")
label3 = tk.Label(root, text="Label 3", bg="blue")

# Pack widgets
label1.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
label2.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
label3.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

root.mainloop()
