### Widgets

In Tkinter, widgets are the building blocks of graphical user interfaces (GUIs). They are visual elements that users interact with, such as buttons, labels, text entry fields, and scrollbars. Each widget serves a specific purpose, whether it's displaying information, accepting user input, or triggering actions.

Widgets are created and manipulated within Tkinter applications to design the layout and functionality of the user interface. They can be configured with various properties, such as size, color, font, and behavior, to customize their appearance and how they respond to user actions.

Tkinter provides a wide range of pre-built widgets, making it easy for developers to create GUIs for their Python applications. By arranging and configuring these widgets, developers can design intuitive and user-friendly interfaces for their programs, enhancing the overall user experience.

| **Widget**      | **Definition**                                                                                                                                 |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| **Button**      | A clickable button that can perform an action or command when pressed.                                                                         |
| **Label**       | A widget used to display text or images.                                                                                                       |
| **Entry**       | A single-line text input field.                                                                                                                |
| **Text**        | A multi-line text input field.                                                                                                                 |
| **Checkbutton** | A checkbox that allows the user to select or deselect an option.                                                                               |
| **Radiobutton** | A button that allows the user to select one option from a set of mutually exclusive options.                                                   |
| **Listbox**     | A widget that displays a list of options from which the user can select one or more.                                                           |
| **Combobox**    | A combination of an entry field and a dropdown list.                                                                                           |
| **Scale**       | A slider that allows the user to select a value from a range.                                                                                  |
| **Spinbox**     | A widget that allows the user to select a value from a range by clicking up and down arrows.                                                   |
| **Frame**       | A container widget used to group other widgets together.                                                                                       |
| **Canvas**      | A widget used for drawing shapes, lines, and other graphics.                                                                                   |
| **Menu**        | A widget that provides a menu bar for the application, with dropdown menus for various commands.                                               |
| **Menubutton**  | A button that, when clicked, displays a menu of choices.                                                                                       |
| **Scrollbar**   | A widget that allows the user to scroll through another widget's content, such as a Text or Listbox.                                           |
| **PanedWindow** | A container widget that can be divided into two or more resizable panes.                                                                       |
| **LabelFrame**  | A container widget that acts as a frame with a title.                                                                                          |
| **Message**     | A widget used to display multiline text, similar to a Label but optimized for longer messages.                                                 |
| **Toplevel**    | A window that is separate from the main application window, often used for dialogs and additional application windows.                         |
| **Progressbar** | A widget that shows the progress of a lengthy operation by filling a bar incrementally.                                                        |

---
### Widget Arguments

| **Widget**      | **Arguments**                                                                                                  |
|-----------------|----------------------------------------------------------------------------------------------------------------|
| **Button**      | `master, text, command, state, width, height, bg, fg, font, image, padx, pady`                                |
| **Label**       | `master, text, image, bg, fg, font, width, height, anchor, padx, pady, relief`                                |
| **Entry**       | `master, textvariable, show, state, width, font, bg, fg, justify`                                             |
| **Text**        | `master, height, width, bg, fg, font, wrap, padx, pady`                                                       |
| **Checkbutton** | `master, text, variable, onvalue, offvalue, command, bg, fg, font, state, padx, pady`                         |
| **Radiobutton** | `master, text, variable, value, command, bg, fg, font, state, padx, pady`                                     |
| **Listbox**     | `master, listvariable, height, selectmode, bg, fg, font, width`                                               |
| **Combobox**    | `master, textvariable, values, state, width, height, postcommand`                                             |
| **Scale**       | `master, from_, to, orient, length, tickinterval, resolution, command, label, showvalue`                      |
| **Spinbox**     | `master, from_, to, increment, textvariable, command, width, state, justify`                                  |
| **Frame**       | `master, bg, bd, relief, padx, pady`                                                                          |
| **Canvas**      | `master, bg, bd, relief, height, width, scrollregion`                                                         |
| **Menu**        | `master, tearoff, bg, fg, font, postcommand`                                                                  |
| **Menubutton**  | `master, text, direction, indicatoron, menu, bg, fg, font, padx, pady, relief`                                |
| **Scrollbar**   | `master, orient, command, bg, troughcolor, width`                                                             |
| **PanedWindow** | `master, orient, sashrelief, sashwidth, sashpad, showhandle, handlesize, bg, bd`                              |
| **LabelFrame**  | `master, text, labelanchor, bg, fg, font, padx, pady, relief`                                                 |
| **Message**     | `master, text, bg, fg, font, width, anchor, padx, pady`                                                       |
| **Toplevel**    | `master, bg, bd, relief, padx, pady`                                                                          |
| **Progressbar** | `master, orient, length, mode, maximum, value, variable`                                                      |

These arguments can be specified when creating a widget to customize its appearance and behavior. Note that `master` refers to the parent widget in which the new widget will be placed.