import tkinter as tk

window = tk.Tk()

window.geometry("300x300")

window.title("Simple Window and Label")

label = tk.Label(window, text="Hello, Tkinter!", font=("Times New Roman", 20))

label.place(x=100, y=150)

window.mainloop()